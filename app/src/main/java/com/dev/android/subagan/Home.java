package com.dev.android.subagan;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    private ImageButton imageButton_resultados;
    private ImageButton imageButton_lotes;
    private ImageButton imageButton_mic;
    private ImageButton imageButton_vivo;
    private static final int INTERVALO = 2000; //2 segundos para salir
    private long tiempoPrimerClick;

    public static String URL = "";
    private BroadcastReceiver bR;
    String url = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent main_activity = new Intent(getApplicationContext(), MainActivity.class);
        main_activity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = ""+getIntent().getExtras().getString(key);

                Log.d("TAG", key + "=" + value);
                switch (key) {
                    case "url":
                        main_activity.putExtra("url", value);
                        startActivity(main_activity);
                        finish();
                        break;
                }
            }

        }
        bR = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                url = intent.getStringExtra("key_url");
                String title = intent.getStringExtra("key_title");
                String body = intent.getStringExtra("key_body");
                /*main_activity.putExtra("url", url);
                startActivity(main_activity);
                finish();*/
                //if (!isFinishing()) alert(url, title, body, "Si", "No");
            }
        };


        imageButton_resultados = (ImageButton) findViewById(R.id.imageButton_resultado);
        imageButton_lotes = (ImageButton) findViewById(R.id.imageButton_lotes);
        imageButton_mic = (ImageButton) findViewById(R.id.imageButton_mic);
        imageButton_vivo = (ImageButton) findViewById(R.id.imageButton_vivo);



        imageButton_resultados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("key_resultado", "resultado");
                startActivity(intent);
                finish();
            }
        });

        imageButton_lotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("key_lotes", "lotes");
                startActivity(intent);finish();
            }
        });

        imageButton_mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("key_mic", "mic");
                startActivity(intent);finish();
            }
        });

        imageButton_vivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("key_vivo", "vivo");
                startActivity(intent);finish();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(bR, new IntentFilter(URL));
    }

    // Notificacion firebase en foreground
    private void alert(final String url, String title, String body, String btnSi, String btnNo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                Home.this);

        // set title
        alertDialogBuilder.setTitle(title);
        // set dialog message
        alertDialogBuilder
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(btnSi, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        Intent intent = new Intent(Home.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("url", url);
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.cancel(0);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton(btnNo, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.cancel(0);
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    //Eliminar notificacion
    public void deletNotification(){
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(0);
    }

    // Avisar al usuario para salir de la app
    @Override
    public void onBackPressed() {
        if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(this, "Presiona de nuevo para salir", Toast.LENGTH_SHORT).show();
        }
        tiempoPrimerClick = System.currentTimeMillis();
    }
}
