package com.dev.android.subagan;

/**
 * Created by MrMonterrosa on 19/09/2017.
 */

public class Const {

    public static final String URL_PAGE_SERVER = "http://subagan.com/";

    public static final String URL_PAGE_LOT_HEADER = URL_PAGE_SERVER+"site/index.php/lot-headers";

    public static final String URL_PAGE_AUCTIONS = URL_PAGE_SERVER+"site/index.php/auctions";

    public static final String URL_PAGE_MIC = URL_PAGE_SERVER+"sistemamic";

    public static final String URL_PAGE_LIVE = URL_PAGE_SERVER+"envivo";

    //public static final String URL_PAGE_WELCOME = "file:///android_asset/welcome/index.html";

}
