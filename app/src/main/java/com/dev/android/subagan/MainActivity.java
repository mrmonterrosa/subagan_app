package com.dev.android.subagan;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private ProgressBar progressBar;
    BottomNavigationView navigation;

    public static String URL = "";
    private BroadcastReceiver bR;
    String _url = null;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent_home = new Intent(getApplicationContext(), Home.class);
                    intent_home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_home);
                    finish();
                    return true;
                case R.id.navigation_utlimos_resultados:
                    cambiar_url(Const.URL_PAGE_AUCTIONS);
                    return true;
                case R.id.navigation_lotes:
                    cambiar_url(Const.URL_PAGE_LOT_HEADER);
                    return true;
                case R.id.navigation_mic:
                    cambiar_url(Const.URL_PAGE_MIC);
                    return true;
                case R.id.navigation_subasta_vivo:
                    cambiar_url(Const.URL_PAGE_LIVE);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        String token = FirebaseInstanceId.getInstance().getToken();

        Log.d("Token: ", "Token: " + token);

        bR = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String url = intent.getStringExtra("key_url");
                String title = intent.getStringExtra("key_title");
                String body = intent.getStringExtra("key_body");
                //if(!isFinishing()) alert(url, title, body, "Si", "No");
            }
        };

        webView = (WebView) findViewById(R.id.web_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        configurar_web_view();


        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.d("TAG", key + "=" + value);
                switch (key) {
                    case "url":
                        _url = value;
                        break;
                }

            }
        }

        //Validar que es una url
        if (_url != null) {
            Pattern pattern = Pattern.compile("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Matcher m = pattern.matcher(_url);

            if (m.matches()) {
                cambiar_url(_url);
            }
        }
        selectMenuExtras();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(bR, new IntentFilter(URL));
    }

    // Seleccionar Opcion de menu
    public void selectMenuExtras() {
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = ""+getIntent().getExtras().getString(key);
                Log.d("TAG", key + "=" + value);
                switch (key) {
                    case "key_resultado":
                        navigation.getMenu().findItem(R.id.navigation_utlimos_resultados).setChecked(true);
                        cambiar_url(Const.URL_PAGE_AUCTIONS);
                        break;
                    case "key_lotes":
                        navigation.getMenu().findItem(R.id.navigation_lotes).setChecked(true);
                        cambiar_url(Const.URL_PAGE_LOT_HEADER);
                        break;
                    case "key_mic":
                        navigation.getMenu().findItem(R.id.navigation_mic).setChecked(true);
                        cambiar_url(Const.URL_PAGE_MIC);
                        break;
                    case "key_vivo":
                        navigation.getMenu().findItem(R.id.navigation_subasta_vivo).setChecked(true);
                        cambiar_url(Const.URL_PAGE_LIVE);
                        break;
                    default:
                        navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
                        navigation.getMenu().findItem(R.id.navigation_home).setChecked(false);
                        break;
                }

            }
        } else {
            navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
        }
    }



    // cambio de url de la pagina en el web view
    public void cambiar_url(String url) {
        webView.loadUrl(url);
    }

    // configuración inicial del web view
    public void configurar_web_view() {
        webView.setFocusable(true);
        webView.setFocusableInTouchMode(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    // Notificacion firebase en foreground
    public void alert(final String url, String title, String body, String btnSi, String btnNo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(title);
        // set dialog message
        alertDialogBuilder
                .setMessage(body)
                .setCancelable(false)
                .setPositiveButton(btnSi, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        cambiar_url(url);
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.cancel(0);
                    }
                })
                .setNegativeButton(btnNo, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.cancel(0);
                        dialog.cancel();

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    /*
    // Devolverse a la pagina anterior en el web view
     */
    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }*/

}
