package com.dev.android.subagan;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by MrMonterrosa on 21/09/2017.
 */

public class FCMInstanceIdService extends FirebaseInstanceIdService {

    private final String TAG = "PUSH";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,"Token: "+token);

        enviarTokenAlServidor(token);

    }

    private void enviarTokenAlServidor(String token) {
        // Enviar token al servidor
    }
}
